Each answer is worth 10 points.
 
 
 1. To solve the exam you must:
     a) Create a fork of my repository with EXACTLY the same name so the script can find it.
     b) Give me READ-WRITE access (so I can push my comments).
     c) Half of the exam is in master, the other part is in branch1, merge the branchs (not rebase).
        COPY-PASTE THE CONTENT = -20 pts.
     d) Edit this file to answer the questions.
 
 -----------------------------------------------------------------------------Secci�n 1
+2. When you use standard library functions (printf for example), in which segment
+   is the address of that function?
+
+3. Explain the difference between merge and rebase (in git)?
+
+4. Why zero terminated strings are one of the worst decisions in the history of computing?
+
+5. Look at the included program mystery.c.
+   If it is initializing an array with consecutive values, explain why it prints interleaved 
+   0s and numbers.
 
 -----------------------------------------------------------------------------Secci�n 2
