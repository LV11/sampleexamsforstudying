#include <stdio.h>
#define I2S(x) ((short *) x)

int main() {
    int i, x[10];

    for(i=0; i<10; i++) {
        x[i] = i;
    }

    for(i=0; i<20; i++) {
        printf("%d\n", *I2S(x+i));
    }
    
}
