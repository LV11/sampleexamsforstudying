#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <unistd.h>

#include "shared_memory.h"

#define MSGSIZE 1024

void serve(int s) {
    char buffer[MSGSIZE];
    int size, i=0;
    struct stat buf;

    FILE *sin = fdopen(s, "r");
    FILE *sout = fdopen(s, "w");

    // Reads the request from the client
    while( fgets(buffer, MSGSIZE, sin) != NULL ) {
        // printf("%d - [%s]\n", ++i, buffer);
        // A blank line is found -> end of headers
        if(buffer[0] == '\r' && buffer[1] == '\n') {
            break;
        }
    }
    sleep(1);

    // Builds response
    fprintf(sout, "HTTP/1.0 200 OK\r\n");
    fprintf(sout, "Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n");
    fprintf(sout, "Content-Type: image/jpeg\r\n");

    stat(get_shared_ptr(), &buf);
    // printf("Size -----------> %d\n", (int)buf.st_size);

    fprintf(sout, "Content-Length: %d\r\n", (int)buf.st_size);
    fprintf(sout, "\r\n");

    FILE *fin = fopen(get_shared_ptr(), "r");
    while ( (size = fread(buffer, 1, MSGSIZE, fin)) != 0) {
        size = fwrite(buffer, 1, size, sout);
    }

    fflush(0);
}

void usage(char *argv0) {
    printf("The port number is required\n");
    printf("$ %s <number>\n", argv0);
}

void handler(int s) {
    // Here you need to change the name of the file
}

int main(int argc, char *argv[]) {
    int sd, sdo, size, r;
    struct sockaddr_in sin, pin;
    socklen_t addrlen;
    int port;
    
    signal(SIGUSR1, SIG_IN);
    
    shared_memory_init();

    if (argc < 1) {
        usage(argv[0]);
        exit(0);
    }
    port = atoi(argv[1]);

    if (!fork()) {
        // A waiting process to change the file to return
        // (Your signal(3) comes here);
        while(pause());

        /* Never reached */
    }

    // 1. Create the socket
    sd = socket(AF_INET, SOCK_STREAM, 0);

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(port);

    // 2. Bind a socket to the port
    r = bind(sd, (struct sockaddr *) &sin, sizeof(sin));
    if (r < 0) {
        perror("bind");
        return -1;
    }
    // 3. Set the backlog
    listen(sd, 5);

    addrlen = sizeof(pin);
    // 4. Wait for the connection
    while( (sdo = accept(sd, (struct sockaddr *)  &pin, &addrlen)) > 0) {
        if(!fork()) {
            // printf("Connected from %s\n", inet_ntoa(pin.sin_addr));
            // printf("Port %d\n", ntohs(pin.sin_port));

            serve(sdo);

            close(sdo);
            exit(0);
        }
    }
    close(sd);

    sleep(1);

}
