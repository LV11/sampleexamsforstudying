1. Git usage (10 pts).

2. HTTP web server with signals (server1.c).
   You have almost the same web server that I explained in class (dummy multiprocess
   web server that returns only a file to the browser).

   You need to provide the port number in the command line (line 75).
   The name of the file to be returned needs to be put in shared memory, 
   but don't worry you only need to use the function get_shared_ptr() to 
   get the relevant pointer. Just don't delete the initialization in line 69.
   I almost forgot, those functions are in a static library libshared.a

   Your task is to set up a signal handler for SIGUSR1, and then from the
   command line you can send the signal to the server to change the file.

   The signal handler is in line 59, but need to be setup around line 79.
   That code needs to be in another process (in line 77) as the main server 
   is waiting with accept in line 104.

   You have 5 jpg files, put the names in an array so you can select a random one
   with rand()%5. Tip: get_shared_ptr() is just a char ptr (in shared memory), 
   just use it with strncpy to change the file, etc..

   To get the id of your process use: ps -f --forest.

   Remember the signals can be sent with kill.

   Points:
     * Array of names of files correctly declared and used (10 pts).
     * Set up and implementation of signal handler with random selection (20 pts).
     * Modifying shared memory and final result tested in the browser (20 pts).
  

3. Dynamic web service (20 pts).
   Ok, static web pages are sooooo boring. With 4 lines (really?) modify server2.c
   in line 33 to call "python36 page.py" using popen and return the response to the
   web server. Now your web server can run python programs too.

   I don't recommend wasting your time reading all the code again, it's the same
   web server minus the fancy stuff for previous question.

4. Makefile with commands to link with the library on q2 and to compile q3 (20 pts).
