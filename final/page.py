from string import Template
from datetime import datetime

page = """<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

  </head>

  <body>
   <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-3">Hola mundo! 今日は世界!</h1>
          <p>En qué momento html se volvió tan complicado! Pero ok, todavía se pueden hacer páginas que se vean bien con "poco" texto.</p>
          <p>Contenido dinámico (haz reload): $fechahora</p>
          <p><a class="btn btn-primary btn-lg" href="#" role="button">Hacer algo...</a></p>
        </div>
      </div>
    </main>

    <footer class="container">
      <p>&copy; ITESM-CCM 2018</p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>
"""

def main():
    p = Template(page).substitute(fechahora = datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    print("HTTP/1.0 200 OK\n", end='')
    print("Date: Fri, 31 Dec 1999 23:59:59 GMT\n", end='')
    print("Content-Type: text/html\n", end='')
    print("Content-Length: %d\n" % len(p.encode("utf-8")), end='')
    print("\n", end='')
    print(p, end='')



if __name__ == "__main__":
    main()
