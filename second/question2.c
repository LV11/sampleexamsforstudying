include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	void (*THIS_IS_THE_FUNCTION_THAT_YOU_NEED_TO_CALL)(void);
	void *lib;
	lib = dlopen("/usr/lib/libQuestion1.so", RTLD_NOW);
	THIS_IS_THE_FUNCTION_THAT_YOU_NEED_TO_CALL = (void (*)(void))dlsym(lib, "THIS_IS_THE_FUNCTION_THAT_YOU_NEED_TO_CALL");
	THIS_IS_THE_FUNCTION_THAT_YOU_NEED_TO_CALL();
	void (*send_to_twitter)(char *);
	void *lib2;
	lib = dlopen("/usr/lib/libQuestion1.so", RTLD_NOW);
	send_to_twitter = (void (*)(char *))dlsym(lib2, "send_to_twitter");
	char *message = "Hola";
	send_to_twitter(message);
}
