1. Git - I need to find your exam in the SAME repository (yours) that the 
first partial, be sure to put it on the main branch.
10 pts

2a and 2b. Running code from a dynamic loaded library.
Somebody (me) installed the libQuestion1.so library in /usr/lib, it is
a non-standard path. Inspect the contents of it so you know what to do
next (calling a function of type void f()). Do your work in the question2.c file.
30 pts

3. Files using standard I/O.
You will decode the content of stair.txt using rot13. You have libDecode.so
and decode.h. Use it in the question3.c file, there you will find comments on
how to proceed.
30 pts

4. Fork/signal and libs, work in question4.c.
* Create a dynamic loading library with the function random_saying of sayings.c 10 pts.
* Create a pool of 4 processes (brothers). Keep one of the ids (remember fork gives 
  that as a result to the parent). They will have a ring of communication with pipes. 10 pts
* Every child process need to have a signal handler for SIGUSR1, after 5 seconds the parent
  decides send SIGUSR1 to the id of the child it stored. Next the child prints a random_saying
  (dynamic loaded function) and write to the pipe to awake the next child. When children
  is awaken they will print the random_saying, write to the pipe (for the next process)
  and exit 10 pts.
* The parent must be asleep until the end (i.e. use wait).
30 pts


EXTRA - answer in this file, 10 points each.
1. Why did Linus Torvalds started Linux? What's his other very successful creation?
	R= Because he wanted to use the functions of his new PC with an 80386 processor. Git :)
2. Why did Linux receive critics as being obsolete? (In it's beginnings).
	R= Monolitic Kernel and no portability
